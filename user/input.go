package user

type RegisterUserInput struct {
	Username string
	Email    string
	Password string
}
